const server = require('fastify')({ logger: true });

server.get('/', (req, rep) => {
  rep.send({ message: "OK" });
});

server.listen(3000);