# Fastify Example
Going over some of the basics of a Fastify API.  
These examples use Visual Studio Code using a bash terminal.

## Walkthrough

### Setup Project Directory
Create working directory for the API:
```
mkdir fastify-example && cd fastify-example
```

### Initialize NPM With Defaults
```
npm init -y
```

### Install Express
```
npm i fastify
```

### Create Server
Create a new file, server.js:
```
touch server.js
```
Add the following content to server.js:
```js
const server = require('fastify')({ logger: true });

server.get('/', (req, rep) => {
  rep.send({ message: "OK" });
});

server.listen(3000);
```

### Start Server
```
node server
```
You should see the message in your terminal:
```
{"level":30,"time":1590620565625,"pid":10536,"hostname":"Codes","msg":"Server listening at http://127.0.0.1:3000","v":1}
```
Open up localhost:3000 as you would any website in your web browser and you will see:
```js
{"message":"OK"}
```

### Congratulations
You just wrote your first JSON API using Fastify.  
Sure it only has one endpoint, the root '/' route.  
This is a starting point, more to come.


